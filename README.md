Wikipedia Data Processing
=========================

This processes every link on wikipedia.
It collects the synonyms, which are the variations in the linked text for each article.
It collects the top 100 features from the prompted model.

Requirements
------------

This requires python 3.9, poetry, curl, jq and make.

Running
-------

The work is split into four parts:

 * Download the data from wikipedia
   This is written to the `data/external/enwiki/DATE` folders.

 * Extract the articles from wikipedia
   This is written to the `data/interim/enwiki/DATE` folders.

 * Extract synonyms from articles
   This is written to the `data/processed/enwiki/DATE/synonyms` folders.
   The aggregated version is written to `data/processed/enwiki/DATE/synonyms.gz.parquet`

 * Determine features for links
   This is written to the `data/processed/enwiki/DATE/features` folders.

Make dependencies are used to run these, so you can generate the synonyms with:

```bash
make data/processed/enwiki/20220701/synonyms.gz.parquet
```

And you can generate the features with:

```bash
make data/processed/enwiki/20220701/features/_SUCCESS
```

 * The download of wikipedia is about 20G and will download one file at a time (be nice!).
 * Extracting the articles takes more than an hour.
 * Extracting the synonyms takes about 10 minutes.
 * Extracting the features takes 20 **days**.

If you can run feature extraction across multiple GPUs then the time will scale linearly.
I've run it for around a day and extracted 2% of the features.
