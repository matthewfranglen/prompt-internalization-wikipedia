from typing import Callable, List, TypeVar

from tqdm.auto import tqdm

import ray

T = TypeVar("T")
V = TypeVar("V")


def parallel(
    values: List[T], function: Callable[[T], V], concurrent_tasks: int, **kwargs
) -> List[V]:
    assert concurrent_tasks > 0
    if concurrent_tasks == 1:
        return _sequential(values=values, function=function, **kwargs)
    return _parallel(
        values=values, function=function, concurrent_tasks=concurrent_tasks, **kwargs
    )


def _sequential(values: List[T], function: Callable[[T], V], **kwargs) -> List[V]:
    return [function(value, **kwargs) for value in tqdm(values)]


def _parallel(
    values: List[T], function: Callable[[T], V], concurrent_tasks: int, **kwargs
) -> List[V]:
    ray.init(ignore_reinit_error=True)

    remote_function = ray.remote(function)
    tasks = []
    results = []
    with tqdm(total=len(values)) as progress:
        while values or tasks:
            while values and len(tasks) < concurrent_tasks:
                tasks.append(remote_function.remote(values.pop(), **kwargs))
            completed, tasks = ray.wait(tasks)
            results.extend(ray.get(completed))
            progress.update(len(completed))
    return results
