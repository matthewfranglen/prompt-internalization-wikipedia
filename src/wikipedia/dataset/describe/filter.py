import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors


class DBSCANFilter:
    """
    This filters the points to those that would be considered part of the majority DBSCAN cluster.
    """

    def __init__(self, min_samples_ratio: float) -> None:
        self.min_samples_ratio = min_samples_ratio
        self.dbscan = None
        self.label = None

    def fit_transform(self, probability: np.ndarray) -> np.ndarray:
        self.fit(probability)
        mask = self.transform(probability)
        return probability[mask]

    def fit(self, probability: np.ndarray) -> None:
        min_samples = max(1, int(probability.shape[0] * self.min_samples_ratio))

        neighbors = NearestNeighbors(n_neighbors=min_samples)
        neighbors.fit(probability)

        neighbor_distance, _ = neighbors.kneighbors(
            X=probability, n_neighbors=min_samples
        )
        # 75% quantile distance that encompasses all min_samples points
        median_eps = np.quantile(neighbor_distance[:, -1], 0.75)

        self.dbscan = DBSCAN(eps=median_eps, min_samples=min_samples)
        self.dbscan.fit(probability)

        labels = pd.Series(self.dbscan.labels_)
        labels = labels[labels != -1]
        self.label = labels.value_counts().index[0]

    def transform(self, points: np.ndarray) -> np.ndarray:
        assert self.dbscan is not None, "call fit first"

        point_count = points.shape[0]
        result = np.ones(shape=point_count, dtype=int) * -1
        for i in range(point_count):
            difference = self.dbscan.components_ - points[i, :]
            distance = np.linalg.norm(difference, axis=1)
            closest_idx = np.argmin(distance)
            if distance[closest_idx] < self.dbscan.eps:
                result[i] = self.dbscan.labels_[
                    self.dbscan.core_sample_indices_[closest_idx]
                ]
        return result == self.label
