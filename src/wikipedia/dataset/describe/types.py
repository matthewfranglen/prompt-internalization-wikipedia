from __future__ import annotations

from dataclasses import dataclass
from typing import Optional

import numpy as np
from scipy.spatial.distance import euclidean


@dataclass
class ArticleDescription:
    target: str
    indices: np.ndarray
    mean: np.ndarray
    std: np.ndarray

    @staticmethod
    def make(
        target: str,
        probability: np.ndarray,
        indices: Optional[np.ndarray],
        minimum_count: int = 5,
    ) -> ArticleDescription:
        if indices is None:
            indices = np.arange(probability.shape[1])

        # drop any columns which do not have at least minimum_count values
        mask = (~np.isnan(probability)).sum(axis=0) >= minimum_count
        indices = indices[mask]
        probability = probability[:, mask]
        probability = np.nan_to_num(probability, nan=0.0)

        mean = np.mean(probability, axis=0)
        std = np.std(probability, axis=0)
        std[std == 0] = std[std != 0].min()

        return ArticleDescription(
            target=target, indices=indices.astype(int), mean=mean, std=std
        )

    def distance(self, point: np.ndarray, weight: bool = False) -> float:
        """Calculates the euclidean distance between
        the point and the cluster centroid."""
        return np.array(
            [
                euclidean(row, self.mean, w=1 / self.std if weight else None)
                for row in point
            ]
        )
