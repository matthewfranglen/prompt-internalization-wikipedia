import numpy as np
import pandas as pd


def expand(
    probabilities: np.ndarray, indices: np.ndarray, size: int = 250_002
) -> np.ndarray:
    """Expand the indicies and probabilities for a single row to the full 250k tokens"""
    result = np.zeros(shape=size)
    result[indices] = probabilities
    return result


def expand_all(
    probabilities: np.ndarray, indices: np.ndarray, size: int = 250_002
) -> np.ndarray:
    """Expand the indicies and probabilities for several rows to the full 250k tokens"""
    rows = probabilities.shape[0]
    result = np.zeros(shape=(rows, size))
    result.flat[indices + (size * np.arange(rows)[:, None])] = probabilities
    return result


def expand_compact(df: pd.DataFrame) -> (np.ndarray, np.ndarray):
    """Expand the indicies and probabilities for several rows to the minimum number of tokens.
    Returns the indices used so the process can be repeated."""
    unique_tokens = np.sort(df["index"].explode().unique())
    token_map = {token_index: index for index, token_index in enumerate(unique_tokens)}
    rows = len(df)
    token_count = len(unique_tokens)

    def map_tokens(tokens: list[int]) -> list[int]:
        return list(map(token_map.get, tokens))

    token_index = np.array(df["index"].apply(map_tokens).tolist())
    probability = np.zeros((rows, token_count))
    probability.flat[token_index + (token_count * np.arange(rows)[:, None])] = np.array(
        df.probability.tolist()
    )

    return (probability, unique_tokens)


def expand_match(
    probability: np.ndarray, probability_indices: np.ndarray, target_indices: np.ndarray
) -> np.ndarray:
    """Expand the indexed probability matrix to match the target indices.
    Missing values are filled with zero."""
    rows = probability.shape[0]
    columns = target_indices.shape[0]
    result = np.zeros(shape=(rows, columns))

    # the indices of values in probability that are in target
    p_indices = np.where(np.in1d(probability_indices, target_indices))[0]
    # the indices of values in target that are in probability
    t_indices = np.where(np.in1d(target_indices, probability_indices))[0]

    #    a = np.array([1, 3, 5, 7, 9])
    #    b = np.array([3, 4, 5, 6, 7])
    #    np.where(np.in1d(a, b))[0]
    # >> array([1, 2, 3])
    # this is the array of values in a that are also present in b:
    #    a[[1, 2, 3]]
    # >> array([3, 5, 7])

    result[:, t_indices] = probability[:, p_indices]
    return result
