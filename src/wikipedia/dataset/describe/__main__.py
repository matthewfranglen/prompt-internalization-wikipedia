import multiprocessing
from dataclasses import asdict
from pathlib import Path
from typing import Optional

import pandas as pd
import typer

from wikipedia.ray import parallel

from .expand import expand_compact
from .filter import DBSCANFilter
from .types import ArticleDescription


def main(
    *,
    input_folder: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
    minimum_rows: int = typer.Option(20),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-file: {output_file}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    input_files = sorted(input_folder.glob("*/*/*.gz.parquet"))
    descriptions = parallel(
        input_files,
        function=describe,
        concurrent_tasks=concurrent_tasks,
        minimum_rows=minimum_rows,
    )
    descriptions = [
        description for description in descriptions if description is not None
    ]
    descriptions = sorted(descriptions, key=lambda description: description.target)
    df = pd.DataFrame([asdict(description) for description in descriptions])
    df.to_parquet(output_file)


def describe(file: Path, minimum_rows: int) -> Optional[ArticleDescription]:
    df = pd.read_parquet(file)
    if len(df) < minimum_rows:
        return None

    name = str(df.iloc[0].target)
    probability, indices = expand_compact(df=df)
    dbscan = DBSCANFilter(min_samples_ratio=0.1)
    try:
        filtered_probabilities = dbscan.fit_transform(probability)
    except ValueError as exception:
        # eps is 0
        print(f"unable to process: {file} - {exception}")
        return None

    return ArticleDescription.make(
        target=name,
        probability=filtered_probabilities,
        indices=indices,
        minimum_count=5,
    )


if __name__ == "__main__":
    typer.run(main)
