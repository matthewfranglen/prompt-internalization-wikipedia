from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List, TypedDict

import numpy as np
import pandas as pd


def read_article_indices(file: Path) -> Dict[str, int]:
    df = pd.read_parquet(file, columns=["target"])
    return dict(zip(df.target, df.index))


def to_dataset(
    file: Path, target_index: Dict[str, int], minimum_targets: int, maximum_targets: int
) -> pd.DataFrame:
    transformer = Transformer(
        target_index=target_index, maximum_targets=maximum_targets
    )

    df = pd.read_parquet(file)

    df["targets"] = df.targets.apply(transformer.filter_targets)
    length = df.targets.str.len()
    df = df[(length >= minimum_targets) & (length <= maximum_targets)]

    df["label"] = df.targets.apply(transformer.to_matrix)
    df = df.drop(columns=["targets"])

    return df.copy()


class Target(TypedDict):
    start: int
    end: int
    target: str


@dataclass
class Transformer:
    target_index: Dict[str, int]
    maximum_targets: int

    def filter_targets(self, targets: List[Target]) -> List[Target]:
        return [entry for entry in targets if entry["target"] in self.target_index]

    def to_matrix(self, entries: List[Target]) -> List[np.array]:
        result = np.ones(shape=(self.maximum_targets, 3), dtype=int)
        result = result * -1
        for index, entry in enumerate(entries):
            result[index, 0] = entry["start"]
            result[index, 1] = entry["end"]
            result[index, 2] = self.target_index[entry["target"]]
        return result.tolist()
