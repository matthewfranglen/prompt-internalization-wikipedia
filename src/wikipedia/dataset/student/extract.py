from dataclasses import dataclass, replace
from typing import Dict, List, Optional, Tuple, TypedDict

import regex as re
import wikitextparser
from transformers import AutoTokenizer

SENTENCE_PATTERN = re.compile(r"([^.!?\s][^.!?]+[.!?])")


class PreparedLink(TypedDict):
    target: str
    start: int
    end: int


class PreparedInput(TypedDict):
    input_ids: List[int]
    targets: List[PreparedLink]


def get_links(
    text: str,
    tokenizer: AutoTokenizer,
    target_map: Dict[str, str],
) -> Optional[PreparedInput]:
    sentences = _get_sentences(text=text, tokenizer=tokenizer, target_map=target_map)

    batch = []
    rows = []
    length_limit = tokenizer.model_max_length - 2  # special tokens
    for sentence in sentences:
        batch_length = sum(len(entry.input_ids) for entry in batch)
        sentence_length = len(sentence.input_ids)

        if batch_length + sentence_length > length_limit:
            row = _to_row(batch=batch, tokenizer=tokenizer)
            if row["targets"]:
                rows.append(row)

            if sentence_length > length_limit:
                batch = []
                continue

            while batch and batch_length + sentence_length > length_limit:
                batch = batch[1:]
                batch_length = sum(len(entry.input_ids) for entry in batch)
        batch.append(sentence)
    if batch:
        row = _to_row(batch=batch, tokenizer=tokenizer)
        if row["targets"]:
            rows.append(row)

    if not rows:
        return None
    row = max(rows, key=lambda row: (len(row["targets"]), len(row["input_ids"])))
    return row


@dataclass
class Link:
    text: str
    target: str
    start: int
    end: int


@dataclass
class SentenceLinks:
    text: str
    input_ids: List[int]
    links: List[Link]


def _to_row(
    batch: List[SentenceLinks],
    tokenizer: AutoTokenizer,
) -> PreparedInput:
    input_ids = (
        [tokenizer.bos_token_id]
        + sum([entry.input_ids for entry in batch], start=[])
        + [tokenizer.eos_token_id]
    )
    offset = 1  # bos_token
    targets = []
    for entry in batch:
        for link in entry.links:
            target = link.target.casefold().strip()
            targets.append(
                {
                    "target": target,
                    "start": link.start + offset,
                    "end": link.end + offset,
                }
            )
        offset += len(entry.input_ids)

    return {"input_ids": input_ids, "targets": targets}


def _get_sentences(
    text: str,
    tokenizer: AutoTokenizer,
    target_map: Dict[str, str],
) -> List[SentenceLinks]:
    text, links = _get_links(text)
    sentences = [
        {
            "text": match.groups()[0],
            "start": match.span()[0],
            "end": match.span()[1],
        }
        for match in SENTENCE_PATTERN.finditer(text)
    ]
    sentences = [
        {
            **sentence,
            **tokenizer(
                sentence["text"],
                add_special_tokens=False,
                return_attention_mask=False,
                return_offsets_mapping=True,
                verbose=False,
            ),
        }
        for sentence in sentences
    ]

    sentences = [
        SentenceLinks(
            text=sentence["text"],
            input_ids=sentence["input_ids"],
            links=_get_sentence_links(
                start=sentence["start"],
                offsets=sentence["offset_mapping"],
                links=links,
                target_map=target_map,
            ),
        )
        for sentence in sentences
    ]
    return sentences


def _get_sentence_links(
    start: int,
    offsets: List[Tuple[int, int]],
    links: List[Link],
    target_map: Dict[str, str],
) -> List[Link]:
    starts = [s for s, _ in offsets]
    ends = [e for _, e in offsets]
    links = [
        replace(link, start=link.start - start, end=link.end - start) for link in links
    ]
    links = [
        replace(link, start=starts.index(link.start), end=ends.index(link.end) + 1)
        for link in links
        if link.start in starts and link.end in ends
    ]
    links = [
        replace(link, target=target_map.get(link.target.casefold().strip()))
        for link in links
    ]
    return [link for link in links if link.target is not None]


def _get_links(text: str) -> Tuple[str, List[Link]]:
    parsed = wikitextparser.parse(text)
    links = []
    offset = 0
    for link in parsed.wikilinks:
        start, end = link.span
        text = link.plain_text()
        length = len(text)
        links.append(
            Link(
                target=link.target,
                text=text,
                start=start - offset,
                end=start + length - offset,
            )
        )
        offset += end - (start + length)
    return parsed.plain_text(), links
