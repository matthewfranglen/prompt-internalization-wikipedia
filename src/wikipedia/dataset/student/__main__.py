import multiprocessing
from pathlib import Path
from typing import Dict, List

import datasets
import pandas as pd
import typer
from tqdm.auto import tqdm
from transformers import AutoTokenizer

from wikipedia.ray import parallel

from .extract import get_links
from .transform import read_article_indices, to_dataset

app = typer.Typer()


@app.command()
def prepare(
    *,
    input_folder: Path = typer.Option(...),
    targets_file: Path = typer.Option(...),
    site: str = typer.Option(...),
    output_folder: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
    model_name: str = typer.Option("xlm-roberta-base"),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"targets-file: {targets_file}",
                f"site: {site}",
                f"output-folder: {output_folder}",
                f"concurrent-tasks: {concurrent_tasks}",
                f"model-name: {model_name}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    target_map = _read_target_map(targets_file, site=site)
    files = sorted(input_folder.glob("*.gz.parquet"))
    parallel(
        files,
        function=_to_dataset,
        concurrent_tasks=concurrent_tasks,
        output_folder=output_folder,
        model_name=model_name,
        target_map=target_map,
    )


def _read_target_map(target_file: Path, site: str) -> Dict[str, str]:
    df = pd.read_parquet(target_file)
    df = df[df.site == site]
    return {
        row.title.strip().casefold(): row.target.strip().casefold() for row in df.iloc
    }


def _to_dataset(
    input_file: Path,
    output_folder: Path,
    model_name: str,
    target_map: Dict[str, str],
) -> None:
    output_file = output_folder / input_file.name
    if output_file.exists():
        return

    df = pd.read_parquet(input_file)
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    rows = [
        get_links(
            text=article,
            tokenizer=tokenizer,
            target_map=target_map,
        )
        for article in df.body.tolist()
    ]
    rows = [row for row in rows if row is not None]
    df = pd.DataFrame(rows)
    df.to_parquet(output_file, compression="gzip")


@app.command()
def targets(
    *,
    input_folder: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-file: {output_file}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    files = sorted(input_folder.glob("*.gz.parquet"))
    per_file_counts = parallel(
        files,
        function=_extract_targets,
        concurrent_tasks=concurrent_tasks,
    )
    counts = _aggregate_counts(per_file_counts)
    counts.to_parquet(output_file, compression="gzip")


def _extract_targets(input_file: Path) -> Dict[str, int]:
    df = pd.read_parquet(input_file, columns=["targets"])
    df = df.targets.explode()
    df = pd.DataFrame(df.tolist())
    return df.target.value_counts().to_dict()


def _aggregate_counts(per_file_counts: List[Dict[str, int]]) -> pd.DataFrame:
    counts = {}
    for file_counts in tqdm(per_file_counts):
        for target, count in file_counts.items():
            counts[target] = counts.get(target, 0) + count
    return pd.DataFrame(counts.items(), columns=["target", "count"])


@app.command()
def dataset(
    *,
    student_folders: List[Path],
    descriptions_file: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
    minimum_targets: int = typer.Option(5),
    maximum_targets: int = typer.Option(10),
    maximum_rows: int = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"student-folders: {student_folders}",
                f"descriptions-file: {descriptions_file}",
                f"output-file: {output_file}",
                f"minimum-targets: {minimum_targets}",
                f"maximum-targets: {maximum_targets}",
                f"minimum-rows: {maximum_rows:,}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    target_index = read_article_indices(descriptions_file)
    files = [file for folder in student_folders for file in folder.glob("*.gz.parquet")]
    rows = parallel(
        files,
        function=to_dataset,
        concurrent_tasks=concurrent_tasks,
        target_index=target_index,
        minimum_targets=minimum_targets,
        maximum_targets=maximum_targets,
    )
    df = pd.concat(rows)
    if len(df) > maximum_rows:
        df = df.sample(n=maximum_rows, random_state=42)
        df = df.reset_index(drop=True)
    ds = datasets.Dataset.from_pandas(df)
    ds.save_to_disk(output_file)


if __name__ == "__main__":
    app()
