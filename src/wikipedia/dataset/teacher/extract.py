from collections import defaultdict
from dataclasses import dataclass, replace
from typing import Iterator, List, Tuple, TypedDict

import regex as re
import wikitextparser
from transformers import AutoTokenizer

SENTENCE_PATTERN = re.compile(r"([^.!?\s][^.!?]+[.!?])")


class PreparedLink(TypedDict):
    target: str
    input_ids: List[int]


def get_links(
    text: str,
    tokenizer: AutoTokenizer,
    prompt: str = " Pet: Dog, Color: Yellow, Vehicle: Tractor, Fruit: Banana,<mask>: {}",
) -> List[PreparedLink]:
    all_links = list(_get_all_links(text=text, tokenizer=tokenizer, prompt=prompt))
    links_by_index = defaultdict(list)
    for link in all_links:
        links_by_index[link.index].append(link)

    # The middle link for a given index is likely the one with the link in the
    # most central sentence, giving greatest surrounding context.
    links = [links[len(links) // 2] for links in links_by_index.values()]
    return [{"target": link.target, "input_ids": link.input_ids} for link in links]


@dataclass
class PromptedLink:
    index: int
    target: str
    input_ids: List[int]
    start: int


@dataclass
class PromptedSentenceLinks:
    text: str
    input_ids: List[int]
    links: List[PromptedLink]


@dataclass
class LinkTokens:
    index: int
    target: str
    input_ids: List[int]


def _get_all_links(
    text: str, tokenizer: AutoTokenizer, prompt: str
) -> Iterator[LinkTokens]:
    sentences = _get_sentences(text=text, tokenizer=tokenizer, prompt=prompt)
    links = [len(link.input_ids) for sentence in sentences for link in sentence.links]
    if not links:
        return
    max_link_length = max(links)

    batch = []
    length_limit = tokenizer.model_max_length - (max_link_length + 2)  # special tokens
    for sentence in sentences:
        batch_length = sum(len(entry.input_ids) for entry in batch)
        sentence_length = len(sentence.input_ids)

        if batch_length + sentence_length > length_limit:
            yield from _explode_batch(batch=batch, tokenizer=tokenizer)

            if sentence_length > length_limit:
                batch = []
                continue

            while batch and batch_length + sentence_length > length_limit:
                batch = batch[1:]
                batch_length = sum(len(entry.input_ids) for entry in batch)
        batch.append(sentence)
    if batch:
        yield from _explode_batch(batch=batch, tokenizer=tokenizer)


def _explode_batch(
    batch: List[PromptedSentenceLinks],
    tokenizer: AutoTokenizer,
) -> List[LinkTokens]:
    input_ids = [tokenizer.bos_token_id]
    input_ids += sum([entry.input_ids for entry in batch], start=[])
    end = [tokenizer.eos_token_id]

    return [
        LinkTokens(
            index=link.index,
            target=link.target.casefold().strip(),
            input_ids=input_ids + link.input_ids + end,
        )
        for entry in batch
        for link in entry.links
    ]


def _get_sentences(
    text: str,
    tokenizer: AutoTokenizer,
    prompt: str,
) -> List[PromptedSentenceLinks]:
    text, links = _get_prompted_links(text=text, tokenizer=tokenizer, prompt=prompt)
    sentences = [
        {
            "text": match.groups()[0],
            "start": match.span()[0],
            "end": match.span()[1],
        }
        for match in SENTENCE_PATTERN.finditer(text)
    ]
    sentences = [
        {
            **sentence,
            **tokenizer(
                sentence["text"],
                add_special_tokens=False,
                return_attention_mask=False,
                return_offsets_mapping=True,
                verbose=False,
            ),
        }
        for sentence in sentences
    ]

    sentences = [
        PromptedSentenceLinks(
            text=sentence["text"],
            input_ids=sentence["input_ids"],
            links=_get_prompted_sentence_links(
                start=sentence["start"], offsets=sentence["offset_mapping"], links=links
            ),
        )
        for sentence in sentences
    ]
    return sentences


def _get_prompted_sentence_links(
    start: int, offsets: List[Tuple[int, int]], links: List[PromptedLink]
) -> List[PromptedLink]:
    starts = [s for s, _ in offsets]
    links = [replace(link, start=link.start - start) for link in links]
    return [
        replace(link, start=starts.index(link.start))
        for link in links
        if link.start in starts
    ]


def _get_prompted_links(
    text: str, tokenizer: AutoTokenizer, prompt: str
) -> Tuple[str, List[PromptedLink]]:
    parsed = wikitextparser.parse(text)
    links = []
    offset = 0
    for index, link in enumerate(parsed.wikilinks):
        start, end = link.span
        target = link.target
        input_ids = tokenizer(
            _get_prompt(noun=target, prompt=prompt),
            add_special_tokens=False,
            return_attention_mask=False,
        ).input_ids
        links.append(
            PromptedLink(
                index=index,
                target=link.target,
                input_ids=input_ids,
                start=start - offset,
            )
        )
        offset += end - (start + len(link.plain_text()))
    return parsed.plain_text(), links


def _get_prompt(noun: str, prompt: str) -> str:
    noun = noun.strip()
    noun = " ".join(word[0].capitalize() + word[1:] for word in noun.split())
    return prompt.format(noun)
