import json
from collections import defaultdict
from hashlib import md5
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Set

import pandas as pd
from tqdm.auto import tqdm
from wikipedia.ray import parallel


def aggregate_files(
    input_folder: Path,
    output_folder: Path,
    minimum: int,
    maximum: int,
    concurrent_tasks: int,
) -> None:
    files = sorted(input_folder.glob("*.gz.parquet"))
    ignore = set()
    count = defaultdict(lambda: 0)

    with TemporaryDirectory() as work_folder:
        work_folder = Path(work_folder)
        print(f"temporary directory: {work_folder}")

        for file in tqdm(files):
            ignore = process_file(
                file=file,
                ignore=ignore,
                count=count,
                work_folder=work_folder,
                output_folder=output_folder,
                maximum=maximum,
            )

        write_remainder(
            work_folder=work_folder,
            output_folder=output_folder,
            minimum=minimum,
            concurrent_tasks=concurrent_tasks,
        )


def process_file(
    *,
    file: Path,
    ignore: Set[str],
    count: defaultdict,
    work_folder: Path,
    output_folder: Path,
    maximum: int,
) -> Set[str]:
    file_df = pd.read_parquet(file)
    for row in file_df.iloc:
        target = row.target.casefold().strip()
        if target in ignore:
            continue

        name = target_filename(target)
        work_file = work_folder / f"{name}.jsonl"
        work_file.parent.mkdir(exist_ok=True, parents=True)
        with work_file.open("a") as handle:
            data = {"target": row.target, "input_ids": row.input_ids.tolist()}
            handle.write(json.dumps(data) + "\n")
        count[target] += 1
        if count[target] >= maximum:
            write_file(work_file=work_file, output_folder=output_folder)
            ignore.add(target)
    return ignore


def write_remainder(
    *, work_folder: Path, output_folder: Path, minimum: int, concurrent_tasks: int
) -> None:
    files = sorted(work_folder.glob("*/*/*.jsonl"))
    parallel(
        files,
        function=write_file,
        concurrent_tasks=concurrent_tasks,
        output_folder=output_folder,
        minimum=minimum,
    )


def target_filename(target: str) -> str:
    name = md5(target.encode("utf-8")).hexdigest()
    return name[:2] + "/" + name[2:4] + "/" + name[4:]


def write_file(work_file: Path, output_folder: Path, minimum: int = 0) -> None:
    name = file_filename(work_file)
    output_file = output_folder / f"{name}.gz.parquet"
    output_file.parent.mkdir(exist_ok=True, parents=True)
    df = pd.read_json(work_file, lines=True)
    if len(df) >= minimum:
        df.to_parquet(output_file, compression="gzip")
    work_file.unlink()


def file_filename(path: Path) -> str:
    return "/".join(
        [
            path.parents[1].name,
            path.parents[0].name,
            path.stem,
        ]
    )
