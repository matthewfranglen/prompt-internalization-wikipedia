import multiprocessing
from pathlib import Path
from typing import List

import pandas as pd
import typer
from transformers import AutoTokenizer

from wikipedia.ray import parallel

from .aggregate import aggregate_files
from .extract import get_links
from .features.pipeline import Pipeline
from .targets import (
    aggregate_student_target_counts,
    aggregate_teacher_article_counts,
    combine_student_and_teacher,
)

app = typer.Typer()


@app.command()
def infer(
    *,
    targets_file: Path = typer.Option(...),
    targets_count: int = typer.Option(10_000),
    output_folder: Path = typer.Option(...),
    feature_count: int = typer.Option(100),
    batch_size: int = typer.Option(16),
    model_name: str = typer.Option("xlm-roberta-base"),
    concurrent_tasks: int = typer.Option(2),
) -> None:
    print(
        "\n".join(
            [
                f"targets-file: {targets_file}",
                f"targets-count: {targets_count:,}",
                f"feature-count: {feature_count}",
                f"batch-size: {batch_size}",
                f"model-name: {model_name}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    pipeline = Pipeline(
        model_name=model_name,
        feature_count=feature_count,
        batch_size=batch_size,
        output_folder=output_folder,
    )

    df = pd.read_parquet(targets_file)
    files = [Path(file) for file in df.head(targets_count).file]
    parallel(
        files,
        function=lambda file: pipeline(file),  # pylint: disable=unnecessary-lambda
        concurrent_tasks=concurrent_tasks,
    )


@app.command()
def best_articles(
    student_files: List[Path],
    teacher_folder: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"student-files: {student_files}",
                f"teacher-folder: {teacher_folder}",
                f"output-file: {output_file}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    student_df = aggregate_student_target_counts(student_files)
    student_df.to_parquet("student.gz.parquet", compression="gzip")
    teacher_df = aggregate_teacher_article_counts(
        teacher_folder, concurrent_tasks=concurrent_tasks
    )
    teacher_df.to_parquet("teacher.gz.parquet", compression="gzip")
    count_df = combine_student_and_teacher(student_df, teacher_df)
    count_df.to_parquet(output_file)


@app.command()
def aggregate(
    input_folder: Path = typer.Option(...),
    output_folder: Path = typer.Option(...),
    minimum: int = typer.Option(5),
    maximum: int = typer.Option(200),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-folder: {output_folder}",
                f"minimum: {minimum}",
                f"maximum: {maximum}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    aggregate_files(
        input_folder=input_folder,
        output_folder=output_folder,
        minimum=minimum,
        maximum=maximum,
        concurrent_tasks=concurrent_tasks,
    )


@app.command()
def collect(
    input_folder: Path = typer.Option(...),
    output_folder: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
    model_name: str = typer.Option("xlm-roberta-base"),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-folder: {output_folder}",
                f"concurrent-tasks: {concurrent_tasks}",
                f"model-name: {model_name}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    files = sorted(input_folder.glob("*.gz.parquet"))
    parallel(
        files,
        function=_extract,
        concurrent_tasks=concurrent_tasks,
        output_folder=output_folder,
        model_name=model_name,
    )


def _extract(input_file: Path, output_folder: Path, model_name: str) -> None:
    output_file = output_folder / input_file.name
    if output_file.exists():
        return

    df = pd.read_parquet(input_file)
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    rows = [
        row
        for article in df.body.tolist()
        for row in get_links(text=article, tokenizer=tokenizer)
    ]
    df = pd.DataFrame(rows)
    df.to_parquet(output_file, compression="gzip")


if __name__ == "__main__":
    app()
