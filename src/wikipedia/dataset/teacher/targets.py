from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd

from wikipedia.ray import parallel


def aggregate_student_target_counts(files: List[Path]) -> pd.DataFrame:
    df = pd.concat([pd.read_parquet(file) for file in files])
    df = df.groupby("target").agg(sum)
    return df


def aggregate_teacher_article_counts(
    folder: Path, concurrent_tasks: int
) -> pd.DataFrame:
    files = sorted(folder.glob("*/*/*.gz.parquet"))
    rows = parallel(
        files,
        function=_teacher_article_count,
        concurrent_tasks=concurrent_tasks,
    )
    df = pd.DataFrame(rows)

    # The articles '5', '7' and '0.38' are duplicated somehow
    # For now the best fix seems to be to drop any duplicates
    target_counts = df.target.value_counts()
    duplicate_targets = set(target_counts[target_counts > 1].index)
    df = df[~df.target.isin(duplicate_targets)]

    df = df.set_index("target")
    return df


def _teacher_article_count(file: Path) -> Tuple[Dict[str, int]]:
    df = pd.read_parquet(file, columns=["target"])
    return {
        "file": str(file),
        "target": str(df.iloc[0].target),
        "count": len(df),
    }


def combine_student_and_teacher(
    student_df: pd.DataFrame, teacher_df: pd.DataFrame
) -> pd.DataFrame:
    log_count = teacher_df.copy()
    log_count["count"] = np.log(log_count["count"])

    count_df = teacher_df.copy()
    count_df = count_df.rename(columns={"count": "descriptions"})
    count_df["links"] = student_df["count"]
    count_df["score"] = student_df["count"].mul(log_count["count"])
    count_df = count_df.dropna()
    count_df = count_df.sort_values(by="score", ascending=False)
    count_df = count_df.reset_index(drop=False)
    count_df = count_df[["target", "score", "descriptions", "links", "file"]]
    count_df["descriptions"] = count_df.descriptions.astype(int)
    count_df["links"] = count_df.links.astype(int)

    return count_df
