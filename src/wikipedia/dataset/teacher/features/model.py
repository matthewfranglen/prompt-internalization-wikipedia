from typing import Set, Tuple

import numpy as np
import torch
from transformers import AutoModelForMaskedLM
from transformers.tokenization_utils_base import BatchEncoding


class FeatureCalculator:
    def __init__(self, model_name: str, feature_count: int, mask_token_id: int) -> None:
        self.model = AutoModelForMaskedLM.from_pretrained(model_name)
        self.model.eval()
        self.model.cuda()
        self.feature_count = feature_count
        self.mask_token_id = mask_token_id

    @torch.inference_mode()
    def __call__(self, inputs: BatchEncoding) -> Tuple[np.array, np.array]:
        inputs = inputs.to(self.model.device)
        mask_indexes = inputs["input_ids"] == self.mask_token_id
        output = self.model(**inputs)

        predictions = output.logits[mask_indexes]
        prediction_indexes = predictions.argsort(dim=1, descending=True)
        prediction_indexes = prediction_indexes[:, : self.feature_count]
        predictions = predictions.gather(dim=1, index=prediction_indexes)
        # make the predictions after filtering sum to 1
        predictions = predictions.softmax(dim=1)

        return predictions.cpu().numpy(), prediction_indexes.cpu().numpy()
