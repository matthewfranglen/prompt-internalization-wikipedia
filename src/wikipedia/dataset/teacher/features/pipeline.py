from pathlib import Path
from typing import List

import pandas as pd
import ray
from transformers import AutoTokenizer

from .collator import Collator
from .model import FeatureCalculator
from .types import Features


class Pipeline:
    def __init__(
        self,
        *,
        model_name: str,
        feature_count: int,
        batch_size: int,
        output_folder: Path,
    ) -> None:
        self.output_folder = output_folder
        self.batch_size = batch_size

        mask_token_id = AutoTokenizer.from_pretrained(model_name).mask_token_id
        self.collator = Collator(model_name=model_name)
        calculator_class = ray.remote(num_gpus=1)(FeatureCalculator)
        self.feature_calculator = calculator_class.remote(
            model_name=model_name,
            feature_count=feature_count,
            mask_token_id=mask_token_id,
        )

    def __call__(self, file: Path) -> int:
        output_file = (
            self.output_folder / file.parents[1].name / file.parents[0].name / file.name
        )
        if output_file.exists():
            return len(pd.read_parquet(output_file))

        df = pd.read_parquet(file)
        if df.empty:
            return 0

        batch_size = self.batch_size
        features = []

        for index in range(0, len(df), batch_size):
            batch = df.iloc[index : index + batch_size]
            targets = batch.target.tolist()
            input_ids = batch.input_ids.tolist()
            features.extend(self._infer_features(targets=targets, input_ids=input_ids))

        self._write(features=features, output_file=output_file)
        return len(features)

    def _infer_features(
        self, targets: List[str], input_ids: List[List[int]]
    ) -> List[Features]:
        batch = self.collator(input_ids)
        token_probability, token_index = ray.get(
            self.feature_calculator.__call__.remote(batch)
        )
        return [
            {"target": target, "probability": probability, "index": index}
            for target, probability, index in zip(
                targets, token_probability, token_index
            )
        ]

    def _write(self, features: List[Features], output_file: Path) -> None:
        output_file.parent.mkdir(exist_ok=True, parents=True)
        df = pd.DataFrame(features)
        df.to_parquet(output_file, compression="gzip")
