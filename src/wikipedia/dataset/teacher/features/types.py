from typing import List, TypedDict


class Features(TypedDict):
    target: str
    probability: List[float]
    index: List[int]
