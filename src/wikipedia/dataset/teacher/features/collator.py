from typing import List

from transformers import AutoTokenizer
from transformers.tokenization_utils_base import BatchEncoding


class Collator:
    def __init__(self, model_name: str) -> None:
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)

    def __call__(self, rows: List[int]) -> BatchEncoding:
        return self.tokenizer.pad(
            [{"input_ids": row} for row in rows], return_tensors="pt", padding=True
        )
