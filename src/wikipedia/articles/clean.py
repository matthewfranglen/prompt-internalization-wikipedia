from __future__ import annotations

import regex as re
import wikitextparser

TABLE_PATTERN = re.compile(r"(?:{{[^}]*}})|(?:{\|[^}]*(?<=\|)})", flags=re.MULTILINE)
URL_PATTERN = re.compile(
    r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
)
TITLE_PATTERN = re.compile(r"^\s*==+[^=]*?==+\s*$", flags=re.MULTILINE)
LEADING_COLON_OR_HASH = re.compile(r"^[:#]", flags=re.MULTILINE)
HTML_TAG = re.compile(r"<[^>]+>")
MANY_BLANK_LINES = re.compile(r"(\n\s*)+\n+")
MANY_SPACES = re.compile(r"  +")
MARKUP = re.compile(r"[']+")
LIST_ITEM = re.compile(r"^\s*\*.*$", re.MULTILINE)
LINK_ONLY_LINE = re.compile(r"^[^a-zA-Z]*(?:\[\[[^]]*\]\][^a-zA-Z]*)+$", re.MULTILINE)


def clean_wikitext(text: str) -> str:
    text = wikitextparser.parse(text).plain_text(replace_wikilinks=False)
    for pattern in [
        TABLE_PATTERN,
        URL_PATTERN,
        TITLE_PATTERN,
        LEADING_COLON_OR_HASH,
        HTML_TAG,
        MARKUP,
        LIST_ITEM,
        LINK_ONLY_LINE,
        MANY_BLANK_LINES,
        MANY_SPACES,
    ]:
        text = pattern.sub(" ", text)

    return text.strip()
