from dataclasses import dataclass


@dataclass
class Article:
    title: str
    body: str
