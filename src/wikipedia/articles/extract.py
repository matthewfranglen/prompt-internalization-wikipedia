from pathlib import Path
from typing import Iterator, Optional

from lxml.etree import Element
from wikipedia.io import read_pages, xpath

from .clean import clean_wikitext
from .types import Article


def read_articles(file: Path) -> Iterator[Article]:
    for page in read_pages(file):
        try:
            article = _get_article(page)
            if article:
                yield article
        except Exception as exception:  # pylint: disable=broad-except
            print(exception)


def _get_article(element: Element) -> Optional[Article]:
    namespace = xpath("mw:ns", element)
    if namespace is None or namespace.text != "0":
        return None

    redirect = xpath("mw:redirect", element)
    if redirect is not None:
        return None

    text_element = xpath("mw:revision/mw:text", element)
    if text_element is None:
        return None

    title_element = xpath("mw:title", element)
    if title_element is None:
        return None

    body = clean_wikitext(text_element.text)
    return Article(title=title_element.text, body=body)
