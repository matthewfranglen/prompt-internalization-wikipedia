import multiprocessing
from pathlib import Path
from typing import List, Tuple

import pandas as pd
import typer
from wikipedia.ray import parallel

from .extract import read_articles
from .types import Article


def main(
    input_folder: Path = typer.Option(...),
    output_folder: Path = typer.Option(...),
    articles_per_file: int = typer.Option(1_000),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-folder: {output_folder}",
                f"articles-per-file: {articles_per_file}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    files = list(enumerate(sorted(input_folder.glob("*.bz2"))))
    parallel(
        files,
        function=_extract,
        concurrent_tasks=concurrent_tasks,
        articles_per_file=articles_per_file,
        output_folder=output_folder,
    )


def _extract(
    index_file: Tuple[int, Path],
    articles_per_file: int,
    output_folder: Path,
) -> None:
    index, file = index_file
    count = 0
    articles = []
    for article in read_articles(file):
        articles.append(article)
        if len(articles) < articles_per_file:
            continue
        _write(
            articles=articles,
            output_folder=output_folder,
            index=index,
            count=count,
        )
        count += 1
        articles = []

    if articles:
        _write(
            articles=articles,
            output_folder=output_folder,
            index=index,
            count=count,
        )


def _write(
    articles: List[Article], output_folder: Path, index: int, count: int
) -> None:
    file = output_folder / f"article-{index:0>3}-{count:0>9}.gz.parquet"
    if file.exists():
        print(f"{file} already exists!")
        return
    df = pd.DataFrame(
        [{"title": article.title, "body": article.body} for article in articles]
    )
    df.to_parquet(file, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
