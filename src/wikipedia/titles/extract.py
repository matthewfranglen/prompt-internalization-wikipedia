import json
from pathlib import Path
from typing import Dict, Iterator, List

import pandas as pd
from lxml.etree import Element
from wikipedia.io import read_pages, xpath
from wikipedia.ray import parallel


def write_titles(input_file: Path, output_folder: Path) -> None:
    output_file = output_folder / f"{input_file.stem}.gz.parquet"
    if output_file.exists():
        return
    df = pd.DataFrame(_read_titles(input_file))
    df.to_parquet(output_file, compression="gzip")


def write_targets(input_folder: Path, output_file: Path, concurrent_tasks: int) -> None:
    files = sorted(input_folder.glob("*/*/*.gz.parquet"))
    targets = parallel(
        files,
        function=_read_targets,
        concurrent_tasks=concurrent_tasks,
    )
    df = pd.DataFrame({"target": sorted(targets)})
    df.to_parquet(output_file, compression="gzip")


def _read_titles(file: Path) -> Iterator[Dict[str, str]]:
    for page in read_pages(file):
        try:
            yield from _get_titles(page)
        except Exception as exception:  # pylint: disable=broad-except
            print(exception)


def _get_titles(  # pylint: disable=too-many-return-statements
    element: Element,
) -> List[Dict[str, str]]:
    namespace = xpath("mw:ns", element)
    if namespace is None or namespace.text != "0":
        return []

    data_format = xpath("mw:revision/mw:format", element)
    if data_format is None or data_format.text != "application/json":
        return []

    text = xpath("mw:revision/mw:text", element)
    if text is None:
        return []
    if not text.text:
        return []

    try:
        data = json.loads(text.text)
    except:
        print(text.text)
        raise
    if "sitelinks" not in data:
        return []

    sitelinks = data["sitelinks"]
    if not sitelinks:
        return []
    if isinstance(sitelinks, list):
        raise ValueError(f"sitelinks is list: {sitelinks}")

    links = {row["site"]: row["title"] for row in sitelinks.values()}
    if "enwiki" not in links:
        return []

    target = links.pop("enwiki")
    if ":" in target:
        return []

    return [
        {"title": title, "site": site, "target": target}
        for site, title in links.items()
    ]


def _read_targets(file: Path) -> str:
    df = pd.read_parquet(file, columns=["target"])
    target = df.iloc[0].target
    if df.target.dtype != object:
        # 'inf' becomes float64 :(
        return str(target).strip().casefold()
    return target.strip().casefold()
