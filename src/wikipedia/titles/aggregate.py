from pathlib import Path
from typing import List, Set

import pandas as pd
from tqdm.auto import tqdm


def aggregate_files(
    input_folder: Path, targets_file: Path, sites: List[str]
) -> pd.DataFrame:
    sites = set(sites)
    targets = set(pd.read_parquet(targets_file).target.tolist())
    files = sorted(input_folder.glob("*.gz.parquet"))
    results = [_get_rows(file, targets=targets, sites=sites) for file in tqdm(files)]
    whole_df = pd.concat(results)
    whole_df = whole_df.drop_duplicates()
    whole_df = whole_df.sort_values(by=["target", "site"])
    whole_df = whole_df.reset_index(drop=True)
    return whole_df


def _get_rows(file: Path, targets: Set[str], sites: Set[str]) -> pd.DataFrame:
    df = pd.read_parquet(file)
    df["title"] = df.title.str.casefold().str.strip()
    df["target"] = df.target.str.casefold().str.strip()
    df = df[df.target.isin(targets)]
    df = df[df.site.isin(sites)]
    df = df.copy()
    return df
