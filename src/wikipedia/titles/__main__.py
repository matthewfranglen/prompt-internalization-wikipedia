import multiprocessing
from pathlib import Path

import typer
from wikipedia.ray import parallel

from .aggregate import aggregate_files
from .extract import write_targets, write_titles

app = typer.Typer()


@app.command()
def collect_titles(
    input_folder: Path = typer.Option(...),
    output_folder: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-folder: {output_folder}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    files = sorted(input_folder.glob("*.bz2"))
    parallel(
        files,
        function=write_titles,
        concurrent_tasks=concurrent_tasks,
        output_folder=output_folder,
    )


@app.command()
def collect_targets(
    input_folder: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-file: {output_file}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    write_targets(
        input_folder=input_folder,
        output_file=output_file,
        concurrent_tasks=concurrent_tasks,
    )


@app.command()
def aggregate(
    input_folder: Path = typer.Option(...),
    targets_file: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"targets-file: {targets_file}",
                f"output-file: {output_file}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    df = aggregate_files(
        input_folder=input_folder,
        targets_file=targets_file,
        sites=[
            "enwiki",
            "dewiki",
            "eswiki",
            "frwiki",
            "itwiki",
            "jawiki",
            "ptwiki",
            "ruwiki",
        ],
    )
    df.to_parquet(output_file, compression="gzip")


if __name__ == "__main__":
    app()
