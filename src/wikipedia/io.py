import bz2
from pathlib import Path
from typing import Iterator, Optional

from lxml.etree import Element, iterparse

MEDIAWIKI_NAMESPACE = "http://www.mediawiki.org/xml/export-0.10/"


def xpath(path: str, element: Element) -> Optional[Element]:
    elements = element.xpath(path, namespaces={"mw": MEDIAWIKI_NAMESPACE})
    if elements:
        return elements[0]
    return None


def read_pages(file: Path) -> Iterator[Element]:
    with bz2.BZ2File(file, "rb") as handle:
        for _event, element in iterparse(
            handle, tag=f"{{{MEDIAWIKI_NAMESPACE}}}page", events=("end",)
        ):
            yield element
            _clear_memory(element)


def _clear_memory(element: Element) -> None:
    element.clear()
    for ancestor in element.xpath("ancestor-or-self::*"):
        while ancestor.getprevious() is not None:
            del ancestor.getparent()[0]
