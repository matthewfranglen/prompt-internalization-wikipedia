import multiprocessing
from pathlib import Path

import pandas as pd
import typer
from wikipedia.ray import parallel

from .extract import SynonymAggregator


def main(
    input_folder: Path = typer.Option(...),
    output_folder: Path = typer.Option(...),
    concurrent_tasks: int = typer.Option(multiprocessing.cpu_count() * 2),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-folder: {output_folder}",
                f"concurrent-tasks: {concurrent_tasks}",
            ]
        )
    )
    output_folder.mkdir(exist_ok=True, parents=True)

    files = sorted(input_folder.glob("*.gz.parquet"))
    parallel(
        files,
        function=_extract,
        concurrent_tasks=concurrent_tasks,
        output_folder=output_folder,
    )


def _extract(input_file: Path, output_folder: Path) -> None:
    output_file = output_folder / input_file.name
    if output_file.exists():
        return

    df = pd.read_parquet(input_file)
    aggregator = SynonymAggregator()
    for article in df.body.tolist():
        aggregator(article)
    df = pd.DataFrame(aggregator.synonyms())
    df.to_parquet(output_file, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
