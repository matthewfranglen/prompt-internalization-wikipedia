from pathlib import Path

import pandas as pd
import typer
from tqdm.auto import tqdm


def main(
    input_folder: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    print(
        "\n".join(
            [
                f"input-folder: {input_folder}",
                f"output-file: {output_file}",
            ]
        )
    )
    output_file.parent.mkdir(exist_ok=True, parents=True)

    files = sorted(input_folder.glob("*.gz.parquet"))
    df = pd.concat([pd.read_parquet(file) for file in tqdm(files)])
    df = df.groupby(["target", "synonym"]).agg(sum)
    df = df.reset_index()

    df.to_parquet(output_file, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
