from collections import Counter, defaultdict
from typing import Dict, List, Set, Union

import wikitextparser


class SynonymAggregator:
    def __init__(self) -> None:
        self._synonyms = Counter()

    def __call__(self, body: str) -> None:
        article_synonyms = _extract(body)
        self._synonyms.update(
            [
                (target, synonym)
                for target, synonyms in article_synonyms.items()
                for synonym in synonyms
            ]
        )

    def synonyms(self) -> List[Dict[str, Union[str, int]]]:
        return [
            {"target": target, "synonym": synonym, "count": count}
            for (target, synonym), count in self._synonyms.items()
        ]


def _extract(body: str) -> Dict[str, Set[str]]:
    parsed = wikitextparser.parse(body)

    synonyms = defaultdict(set)
    for link in parsed.wikilinks:
        target = _clean(link.target)
        if not target:
            continue
        if ":" in target:
            continue
        synonyms[target].add(target)
        if not link.text:
            continue
        synonym = _clean(link.text)
        synonyms[target].add(synonym)

    return dict(synonyms)


def _clean(text: str) -> str:
    return text.casefold().strip()
