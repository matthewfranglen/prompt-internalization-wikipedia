.PHONY: FORCE

.SECONDARY:

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL := /bin/bash
.SHELLFLAGS = -o pipefail -c
PYTHONPATH = $(PWD)/src

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install python dependencies
requirements : .make/requirements

.make/requirements : pyproject.toml poetry.lock
	poetry install
	touch $@

# The external folder contains the downloaded wikipedia data dumps.

## Download data from wikipedia.
## Example: `make data/external/jawiki/20220701/_SUCCESS` or `make data/external/wikidatawiki/20220701/_SUCCESS`.
## Only a single file is downloaded at once, to be nice.
## Can run multiple targets in one invocation.
data/external/%/_SUCCESS : data/external/%/dumpstatus.json
	for path in $$(jq --raw-output '.jobs.articlesdump.files[].url' data/external/$*/dumpstatus.json); do \
		file=data/external/$*/$$(basename $${path}); \
		if [ ! -e $${file} ]; then \
			wget --output-document $${file} https://dumps.wikimedia.org$${path}; \
		else \
			echo $${file} already downloaded; \
		fi \
	done;
	touch $@

data/external/%/dumpstatus.json :
	if [ ! -e data/external/$* ]; then mkdir -p data/external/$* ; fi
	curl --fail https://dumps.wikimedia.org/$*/dumpstatus.json | jq . > $@
	if [ $$(jq --raw-output .jobs.articlesdump.status $@) != "done" ]; then \
		rm $@; \
		echo wikipedia data not fully dumped yet, try again later >&2; \
		false; \
	fi

# The interim folder contains the various stages of preprocessing the data
# dumps. This includes:
# * Article extraction
# * Link extraction
# * Target extraction
# * Complete dataset extraction
#
# The complete dataset is then formed into a cut down dataset and targets in
# the processed folder. The reduction of the dataset restricts the targets to
# those that are frequent in the student dataset and can be well described.

## Collect article title and body from wikipedia data dumps.
data/interim/%/articles/_SUCCESS : data/external/%/_SUCCESS requirements
	if [ ! -e data/interim/$*/articles ]; then mkdir -p data/interim/$*/articles ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.articles \
			--input-folder data/external/$* \
			--output-folder data/interim/$*/articles
	touch $@

## Prompt and tokenize links from articles for the teacher feature calculation.
data/interim/enwiki/%/teacher/articles/_SUCCESS : data/interim/enwiki/%/articles/_SUCCESS requirements
	if [ ! -e data/interim/enwiki/$*/teacher/articles ]; then mkdir -p data/interim/enwiki/$*/teacher/articles ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.teacher collect \
			--input-folder data/interim/enwiki/$*/articles \
			--output-folder data/interim/enwiki/$*/teacher/articles
	touch $@

## Aggregate the prompted/tokenized articles for teacher feature calculation.
## Collects between 5 and 200 rows per target.
## Took 14h to run.
data/interim/enwiki/%/teacher/dataset/_SUCCESS : data/interim/enwiki/%/teacher/articles/_SUCCESS requirements
	if [ ! -e data/interim/enwiki/$*/teacher/dataset ]; then mkdir -p data/interim/enwiki/$*/teacher/dataset ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.teacher aggregate \
			--input-folder data/interim/enwiki/$*/teacher/articles \
			--output-folder data/interim/enwiki/$*/teacher/dataset
	touch $@

## Collect title data from wikidata.
## This associates all the different language titles for every page in wikidata.
## Took 14m to run.
data/interim/wikidatawiki/%/titles/_SUCCESS : data/external/wikidatawiki/%/_SUCCESS requirements
	if [ ! -e data/interim/wikidatawiki/$*/titles ]; then mkdir -p data/interim/wikidatawiki/$*/titles ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.titles collect-titles \
			--input-folder data/external/wikidatawiki/$* \
			--output-folder data/interim/wikidatawiki/$*/titles
	touch $@

## Aggregate title data into single file with restricted languages.
data/interim/wikidatawiki/%/titles.gz.parquet : data/interim/wikidatawiki/%/titles/_SUCCESS data/interim/wikidatawiki/%/targets.gz.parquet requirements
	if [ ! -e data/interim/wikidatawiki/$* ]; then mkdir -p data/interim/wikidatawiki/$* ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.titles aggregate \
			--input-folder data/interim/wikidatawiki/$*/titles \
			--targets-file data/interim/wikidatawiki/$*/targets.gz.parquet \
			--output-file data/interim/wikidatawiki/$*/titles.gz.parquet
	touch $@

## Collect target articles from wikipedia.
## This makes a list of all known article titles that have enough targets to describe.
data/interim/wikidatawiki/%/targets.gz.parquet : data/interim/enwiki/%/teacher/dataset/_SUCCESS requirements
	if [ ! -e data/processed/wikidatawiki/$* ]; then mkdir -p data/processed/wikidatawiki/$* ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.titles collect-targets \
			--input-folder data/interim/enwiki/$*/teacher/dataset \
			--output-file data/interim/wikidatawiki/$*/targets.gz.parquet
	touch $@

## Collect synonyms from wikipedia.
data/interim/%/synonyms/_SUCCESS : data/interim/%/articles/_SUCCESS requirements
	if [ ! -e data/interim/$*/synonyms ]; then mkdir -p data/interim/$*/synonyms ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.synonyms \
			--input-folder data/interim/$*/articles \
			--output-folder data/interim/$*/synonyms
	touch $@

data/interim/%/student/dataset/_SUCCESS : SITE = $(shell echo $* | cut -d '/' -f 1)
data/interim/%/student/dataset/_SUCCESS : DATE = $(shell echo $* | cut -d '/' -f 2)
data/interim/%/student/dataset/_SUCCESS : data/interim/%/articles/_SUCCESS requirements
	$(MAKE) -s data/interim/wikidatawiki/$(DATE)/titles.gz.parquet
	if [ ! -e data/interim/$*/student/dataset ]; then mkdir -p data/interim/$*/student/dataset ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.student prepare \
			--input-folder data/interim/$*/articles \
			--targets-file data/interim/wikidatawiki/$(DATE)/titles.gz.parquet \
			--site $(SITE) \
			--output-folder data/interim/$*/student/dataset
	touch $@

data/interim/%/student/target-counts.gz.parquet : data/interim/%/student/dataset/_SUCCESS requirements
	if [ ! -e data/interim/$*/student ]; then mkdir -p data/interim/$*/student ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.student targets \
			--input-folder data/interim/$*/student/dataset \
			--output-file data/interim/$*/student/target-counts.gz.parquet
	touch $@


# The processed data folder primarily contains the results of inference. The
# teacher inference is based on the top targets from the students that can be
# well described.

## TODO: Encode the languages in the path
data/processed/%/targets.gz.parquet : LANGUAGES ?= de es fr it ja pt ru
data/processed/%/targets.gz.parquet : $(patsubst %,data/interim/%wiki/$*/student/target-counts.gz.parquet,$(LANGUAGES)) \
                                      data/interim/enwiki/%/teacher/dataset/_SUCCESS \
                                      requirements
	if [ ! -e data/processed/$* ]; then mkdir -p data/processed/$* ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.teacher best-articles \
			$(patsubst %,data/interim/%wiki/$*/student/target-counts.gz.parquet,$(LANGUAGES)) \
			--teacher-folder data/interim/enwiki/$*/teacher/dataset \
			--output-file data/processed/$*/targets.gz.parquet
	touch $@


## Perform inference over the prompted teacher rows.
data/processed/enwiki/%/teacher/features/_SUCCESS : LANGUAGES ?= de es fr it ja pt ru
data/processed/enwiki/%/teacher/features/_SUCCESS : data/processed/%/targets.gz.parquet \
                                                    data/interim/enwiki/%/teacher/dataset/_SUCCESS \
                                                    requirements
	if [ ! -e data/processed/enwiki/$*/teacher/features ]; then mkdir -p data/processed/enwiki/$*/teacher/features ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.teacher infer \
			--targets-file data/processed/$*/targets.gz.parquet \
			--output-folder data/processed/enwiki/$*/teacher/features
	touch $@

## Convert the article features into descriptions.
data/processed/%/article-descriptions.gz.parquet : LANGUAGES ?= de es fr it ja pt ru
data/processed/%/article-descriptions.gz.parquet : data/processed/enwiki/%/teacher/features/_SUCCESS requirements
	if [ ! -e data/processed/$* ]; then mkdir -p data/processed/$* ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.describe \
			--input-folder data/processed/enwiki/$*/teacher/features \
			--output-file data/processed/$*/article-descriptions.gz.parquet
	touch $@

## TODO: Encode the languages in the path
data/processed/%/student/train.dataset : LANGUAGES ?= de fr it ja ru
data/processed/%/student/train.dataset : $(patsubst %,data/interim/%wiki/$*/student/dataset/_SUCCESS,$(LANGUAGES)) \
                                         data/processed/%/article-descriptions.gz.parquet \
                                         requirements
	if [ ! -e data/processed/$*/student ]; then mkdir -p data/processed/$*/student ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.student dataset \
			$(patsubst %,data/interim/%wiki/$*/student/dataset,$(LANGUAGES)) \
			--descriptions-file data/processed/$*/article-descriptions.gz.parquet \
			--output-file data/processed/$*/student/train.dataset \
			--maximum-rows 1000000
	touch $@

data/processed/%/student/valid.dataset : LANGUAGES ?= es pt
data/processed/%/student/valid.dataset : $(patsubst %,data/interim/%wiki/$*/student/dataset/_SUCCESS,$(LANGUAGES)) \
                                         data/processed/%/article-descriptions.gz.parquet \
                                         requirements
	if [ ! -e data/processed/$*/student ]; then mkdir -p data/processed/$*/student ; fi
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.dataset.student dataset \
			$(patsubst %,data/interim/%wiki/$*/student/dataset,$(LANGUAGES)) \
			--descriptions-file data/processed/$*/article-descriptions.gz.parquet \
			--output-file data/processed/$*/student/valid.dataset \
			--maximum-rows 10000
	touch $@


## Aggregate synonym data into single file.
data/processed/%/synonyms.gz.parquet : data/interim/%/synonyms/_SUCCESS requirements
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m wikipedia.synonyms.aggregate \
			--input-folder data/interim/$*/synonyms \
			--output-file data/processed/$*/synonyms.gz.parquet


## Format Python code
format : requirements
	poetry run black src tests scripts
	poetry run isort src tests scripts



FORCE :

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
